<?php

namespace Application\Model;

class User
{
    private $id;
    private $userName;
    private $password;
    private $fullName;

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setUserName($userName)
    {
        $this->userName = $userName;
        return $this;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUserName()
    {
        return $this->userName;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getFullName()
    {
        return $this->fullName;
    }

    public function jsonSerialize() {
        return get_object_vars($this);
    }

    public function sqlSerialize() {
        return [
            'id' => $this->id,
            'user_name' => $this->userName,
            'password' => $this->password,
            'full_name' => $this->fullName,
        ];
    }
}

