<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class UserController extends AbstractRestfulController
{
    protected $userService;
    protected $collectionOptions = ['GET', 'POST'];
    protected $resourceOption = ['GET', 'PUT', 'DELETE'];

    public function __construct($userService)
    {
        $this->userService = $userService;
    }

    public function get($id)
    {
        $user = $this->userService->get($id);
        return new JsonModel($user->jsonSerialize());
    }

    public function create($data)
    {
        $rawBody = json_decode($this->processBodyContent($this->getRequest()), true);
        $user = $this->userService->create($rawBody);
        return new JsonModel($user->jsonSerialize());
    }

    public function loginAction()
    {
        return new JsonModel(
            $this->userService->login($rawBody['userName'], $rawBody['password'])
        );
    }

    public function findAction()
    {
        $rawBody = json_decode($this->processBodyContent($this->getRequest()), true);
        return new JsonModel(
            $this->userService->find($rawBody)
        );
    }

    public function oprions()
    {
        $response = $this->getResponse();

        $response->getHeaders()->addHeaderLine('Allow', implode(',', $this->getOptions()));

        return $response;
    }

    private function getOptions()
    {
        if ($this->params()->fromRoute('id', false)) {
            return $this->resourceOption;
        } else {
            return $this->collectionOptions;
        }
    }
}
