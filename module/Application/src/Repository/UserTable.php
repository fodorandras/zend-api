<?php

namespace Application\Repository;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Sql;

class UserTable
{
    protected $db;

    public function __construct(AdapterInterface $db)
    {
        $this->db = $db;
    }

    public function create($user)
    {
        $sql = new Sql($this->db);

        $statement = $sql
            ->insert()
            ->into('user')
            ->columns(['user_name', 'password', 'full_name'])
            ->values([$user['user_name'], $user['password'], $user['full_name']]);

        return $sql
            ->prepareStatementForSqlObject($statement)
            ->execute()
            ->getGeneratedValue();
    }

    public function get($userId)
    {
        $sql = new Sql($this->db);

        $statement = $sql
            ->select()
            ->from('user')
            ->where(['id' => $userId]);

        return $sql
            ->prepareStatementForSqlObject($statement)
            ->execute()
            ->getResource()
            ->fetch();
    }

    public function find($data)
    {
        $sql = new Sql($this->db);

        $statement = $sql
            ->select()
            ->from('user')
            ->where($data);

        return (array) $sql
            ->prepareStatementForSqlObject($statement)
            ->execute()
            ->getResource()
            ->fetchAll();
    }
}
