<?php

namespace Application\Service;

use Application\Model\User;


class UserService
{
    const SECRET_KEY = "3kdKM4ns@";
    private $userTable;

    public function __construct($userTable)
    {
        $this->userTable = $userTable;
    }

    public function create($user)
    {
        $user = $this->populateUser($user);
        $user->setPassword($this->generatePassword($user->getUserName, $user->getPassword));
        $createdUser = (array) $this->userTable->create($user);
        return $this->populateUser($createdUser[0])->sqlSerialize();
    }

    public function get($userId)
    {
        return $this->populateUser($this->userTable->get($userId));
    }

    public function find($data)
    {
        $user = $this->populateUser($data);
        $result = [];
        foreach ($this->userTable->find($user->sqlSerialize()) as $row) {
            $result[] = $this->populateUser($row)->jsonSerialize();
        }

        return $result;
    }

    public function login($userName, $password)
    {
        $myUser = $this->find([
            'user_name' => $userName,
            'password' => $this->generatePassword($userName, $password)
        ]);

        return ['success' => count($myUser) > 0 ? true : false];
    }

    private function populateUser($tableRow)
    {
        $user = new User();
        return $user
            ->setId($tableRow['id'])
            ->setUserName(isset($tableRow['user_name']) ? $tableRow['user_name'] : $tableRow['userName'])
            ->setPassword($tableRow['password'])
            ->setFullName(isset($tableRow['full_name']) ? $tableRow['full_name'] : $tableRow['fullName']);
    }

    private function generatePassword($userName, $password)
    {
        return md5("$userName/$password/" . $this::SECRET_KEY);
    }

}
