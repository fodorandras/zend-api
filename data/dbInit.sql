CREATE TABLE user (
   id int(11) NOT NULL auto_increment,
   user_name varchar(100) NOT NULL,
   password varchar(100) NOT NULL,
   full_name varchar(100) NOT NULL,
   PRIMARY KEY (id)
 );

INSERT IGNORE INTO user (id, user_name, password, full_name) VALUES (1, 'admin', '...', 'Super Admin');
